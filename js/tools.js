/**
 * String format function
 * Example : 'My name is {name}'.formatUnicorn({name: 'John Doe'});
 * @author stackoverflow.com
 * @type {Function}
 */
String.prototype.formatUnicorn = String.prototype.formatUnicorn ||
    function () {
        "use strict";
        let str = this.toString();
        if (arguments.length) {
            let t = typeof arguments[0];
            let key;
            let args = ("string" === t || "number" === t) ?
                Array.prototype.slice.call(arguments)
                : arguments[0];

            for (key in args) {
                str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
            }
        }

        return str;
    };

/**
 * Create an HTML element from an HTML string
 * @type {Function}
 */
String.prototype.toHtmlElement = String.prototype.toHtmlElement ||
    function() {
        let str = this.toString().trim();
        let template = document.createElement('template');
        template.innerHTML = str;
        return template.content.firstChild;
    };


/**
 * Allow object iteration
 * @param o Object to be iterated
 */
function* iterate_object(o) {
    const keys = Object.keys(o);
    for (let i=0; i<keys.length; i++) {
        yield [keys[i], o[keys[i]]];
    }
}

/**
 * Determine if object is null or undefined.
 * Remember to check parent(s) existence/value first
 * @param o Object (?) to check
 */
function null_or_undef(o) {
	return (typeof o === "undefined" || o === null)
}
