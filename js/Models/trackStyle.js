RM.TrackStyle = class {
    constructor(color, opacity, weight, lineCap) {
        this.color   = color  ;
        this.opacity = opacity;
        this.weight  = weight ;
        this.lineCap = lineCap;
    }
};