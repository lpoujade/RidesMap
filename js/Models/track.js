RM.Track = class {
    constructor(title, xml, difficulty, type, id=null) {
        /* weird globals */
        this.map = RM.map;

        /* computed values */
        this._widget = null;
        this._marker = null;
        this._xml = null;
        this._start_point = null;
        this._start_pos = null;
        this._length = null
        this.xmlVersion = undefined;
        this.stringifiedVersion = undefined;

        /* dynamics values */
        this.inList = false;
        this.displayed = false;

        /* static datas */
        this._title = title;
        this.id = id;
        this.string = xml;
        this.difficulty = difficulty;
        this.type = type;
    }

    get fu() {
        return {id: this.id, title: this.title, length: this.length, gain_up: this.gain_up, gain_down: this.gain_down, difficulty: this.difficulty}
    }

    get toJson() {
        return JSON.stringify({
            id: this.id ? this.id : -1,
            name: this.title,
            type: this.type,
            string: this.string,
            difficulty: this.difficulty
        })
    }

    get title() {
        return this._title
    }

    get marker() {
        if (!this._marker) {
            if (!this.type) {
                console.error('[Track.get marker] missing type', this)
                return;
            }
            this._marker = RM.Marker.createStartMarker(this.start_position, this.type, RM.trackActions.view.bind(null, this))
        }
        return this._marker
    }

    get xml() {
        if (!this._xml) {
            console.debug('[Track.get xml] computing xml', this)
            if (!this.string) {
                console.error('[Track.get xml] missing string', this);
                return null;
            }
            this._xml = (new DOMParser).parseFromString(this.string, 'text/xml');
            if (this._xml.documentElement.nodeName == 'parsererror') {
                console.error('[Track.get xml] failed to parse GPX for track ', this);
                this._xml = null;
                return null;
            }
        }
        return this._xml
    }

    get start_position() {
        if (!this._start_pos) {
            console.debug('[Track.get start_position] computing start_position ', this)
            if (!this.xml) {
                console.error('[Track.get start_position] no xml', track)
                return;
            }
            this._start_point = this.xml.getElementsByTagName('trkpt')[0]
                ? this.xml.getElementsByTagName('trkpt')[0]
                : this.xml.getElementsByTagName('rtept')[0];
            if (!this._start_point) {
                console.error('[Track.get start_position] can\'t find start point for ', this)
                return;
            }
            this._start_pos = {
                lat: Number(this._start_point.getAttribute('lat')),
                lng: Number(this._start_point.getAttribute('lon'))
            }
        }
        return this._start_pos;
    }
    get start_point() {
        if (!this._start_point) {
            /* cf getter */
            let pos = this.start_position
        }
        return this._start_point
    }

    get length() {
        if (!this._length) {
            console.debug('[Track.get length] computing length ', this)
            if (!this.xml) { console.error('[Track.get length] missing xml', this); return; }
            if (!this.widget) { console.error('[Track.get length] missing widget', this); return; }
            this._length = (Math.round(this.widget.get_distance()/100)) / 10
        }
        return this._length;
    }

    get gain_up() {
        if (!this._gain_up) {
            console.debug('[Track.get gain_up] computing gain_up ', this)
            this._gain_up = Math.round(this.widget.get_elevation_gain())
        }
        return this._gain_up
    }

    get gain_down() {
        if (!this._gain_down) {
            console.debug('[Track.get gain_down] computing gain_down ', this)
            this._gain_down = Math.round(this.widget.get_elevation_loss())
        }
        return this._gain_down
    }

    get widget() {
        if (!this._widget) {
            console.debug('[Track.get widget] computing widget ', this)
            if (!this.string) {
                console.error('[Track.compute] missing string', this)
                return;
            }
            this.style = RM.RideTypes[this.type] ? RM.RideTypes[this.type].style : RM.RideTypes['muni'].style;
            this._widget = new L.GPX(this.string, {
                /* TODO async */
                async: false,
                polyline_options: this.style,
                marker_options: {
                    startIconUrl: false,
                    endIconUrl: false
                }
            })
        }
        return this._widget
    }

    static getElevations(gpx) {
        gain = gpx.get_elevation_gain()
        loss = gpx.get_elevation_loss()
        return [gain, loss]
    }
    // endregion

    toggleDisplay() {
        const addOrRemove = this.displayed ? 'removeFrom' : 'addTo';
        this.widget[addOrRemove](this.map)
        this.displayed = !this.displayed
    }

    focus_on_map() {
        if (!this.displayed)
            this.toggleDisplay()
        RM.centerMap(this.widget)
    }
}
