/*
 * templates for tracks items on map
 */

Templates.tracks = {
    trackIdSuffix: 'track',
    startMarkerBox: "<div>{name}<br><a id='{id}' href='#' onclick='RM.toggleTrackDisplay({id})'>Toggle track display</a> </div>",
    trackBoxListItem: '<li id="desc_track_{id}" class="table-view-cell media">' +
'<a class="navigate-right" href="#" onclick="RM.goToTrack({id})">' +
'<img class="media-object pull-left" src="http://placehold.it/42x42">' +
'{title}' +
    '<p>{length} km - {difficulty}</p>' +
'<div class="media-body">' +
    '<p>↑ {gain_up} m</p>' +
    '<p>↓ {gain_down} m</p>' +
'</div>' +
'</a>' +
    '</ul>',
    trackList: '<div id="tracklist">'+
'<input type="text" oninput="RM.delayFilterTrack(this)" placeholder="name, type …" />'+
'<ul id="tracklist_ul" class="table-view">'+
'</ul></div>',

    trackFullView:
    `<div id="trackInfosView" data-rm-current-track="{id}">
<button class="closeTrackView" onclick="RM.switchView()">X</button>
<!--<button class="editTrackView" onclick="RM.trackActions.edit({id})">🖉</button>-->
<h2>{title}</h2>
<div class="track_infos">
    <div>
        <p>{length} km</p>
        <p>{difficulty}</p> 
    </div>
    <div>
        <p>↑ {gain_up} m</p> 
        <p>↓ {gain_down} m</p> 
    </div>
    <div>
        <p>{description}</p>
    </div>
</div> 
</div>`
}
