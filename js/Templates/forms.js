Templates.Forms = {};
Templates.Forms.live = {};

Templates.Forms.template = '<form id="{id}" action="{url}"></form>';


Templates.Forms.labelTemplate = '<label for="{input_id}">{text}</label>'
Templates.Forms.formGroupTemplate = '<div class="form-group"></div>'
Templates.Forms.inputTemplate = '<input type="{type}" name="{name}" id="{id}" value="{value}" />'
Templates.Forms.selectTemplate = '<select class="form-control" name="{name}" id="{id}"></select>'
Templates.Forms.optionTemplate = '<option value="{name}">{name}</option>'
Templates.Forms.submitTemplate = '<button class="btn btn-positive btn-block" type="submit" form="{form_id}">{text}</button>'

Templates.Forms.createForm = function(id, action, inputs) {
	let form = Templates.Forms.template.formatUnicorn({id, url: action.url}).toHtmlElement(),
		submit = Templates.Forms.submitTemplate.formatUnicorn({form_id: id, text: RM.l.send}).toHtmlElement()

	form.addEventListener('submit', action.func)
	for (field of inputs) {
		if (field.label) {
			const htmllabel = Templates.Forms.labelTemplate.formatUnicorn({input_id: field.id, text: field.label}).toHtmlElement()
			form.append(htmllabel)
		}

		let formgroup = null

		if (field.type === 'select') {
			selectfield = Templates.Forms.selectTemplate.formatUnicorn({name: field.name, id: field.id}).toHtmlElement()
			formgroup = Templates.Forms.formGroupTemplate.formatUnicorn().toHtmlElement()
			let opt = null
			for (choice of field.choices) {
				opt = Templates.Forms.optionTemplate.formatUnicorn({name: choice}).toHtmlElement()
				if (field.value && field.value === choice)
					opt.selected = true
				selectfield.append(opt)
			}
			formgroup.append(selectfield)
        } else {
			value = field.value ? field.value : '';
			inputdata = {name: field.name, type: field.type, id: field.id, value}
			inputfield = Templates.Forms.inputTemplate.formatUnicorn(inputdata).toHtmlElement()

            if (field.action) {
                inputfield.addEventListener(field.action.evtype, field.action.func)
            }

			formgroup = Templates.Forms.formGroupTemplate.formatUnicorn().toHtmlElement()
			formgroup.append(inputfield)
		}

		form.append(formgroup)
	}
	return [form, submit];
};
