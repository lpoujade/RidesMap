const Templates = {};

Templates.elements = {
    selectListTemplate : '<div class="form-group"><select class="form-control" id="{id}Select"></select></select></div>',
    optionTemplate     : '<option>{text}</option>',
    input              : '<div><label for="{id}-label">{text}</label><input type="{type}"/>'
};

Templates.inputTypes = {
    button       : 'button',
    checkbox     : 'checkbox',
    color        : 'color',
    date         : 'date',
    datetimeLocal: 'datetime-local',
    email        : 'email',
    file         : 'file',
    hidden       : 'hidden',
    image        : 'image',
    month        : 'month',
    number       : 'number',
    password     : 'password',
    radio        : 'radio',
    range        : 'range',
    reset        : 'reset',
    search       : 'search',
    submit       : 'submit',
    tel          : 'tel',
    text         : 'text',
    time         : 'time',
    url          : 'url',
    week         : 'week '
};

Templates.createSelectList = function(id, elements){
    let selectBlock = $(Templates.elements.selectListTemplate.formatUnicorn({id}).toHtmlElement());
    let selectList = $(selectBlock.find('select'));
    for(const text of elements) {
        let option = $(Templates.elements.optionTemplate.formatUnicorn({text}).toHtmlElement());
        selectList.append(option);
    }
    return {selectBlock, selectList};
};

Templates.createInput = function (id, label, type) {
    return $(Templates.elements.selectListTemplate.formatUnicorn({id, label, type}).toHtmlElement());
};

Templates.closeView = '<button onclick="RM.switchView()">X</button>';
