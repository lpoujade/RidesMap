/*
 *	WARN	errors (mailed ?)
 *	INFO	basic info (route accessed ...)
 *	DEBUG	more info
 *	VERB	all (?) infos
 */
const log_levels = {
	'NOTHING': -1,
	'ERROR': 0,
	'WARN': 1,
	'INFO': 2,
	'DEBUG': 3,
	'VERB': 4
}

function log(...params) {
	p = params[0]
	let level = typeof(p) == "number" ? p : process.env.RIDESMAP_LOGLEVEL
	if (log_levels[process.env.MATCHA_LOGLEVEL || 'WARN'] >= log_levels[level]) {
		let ar_len = params.length
		console.log(level[0] + '| ', params[1])
		for (var i = 2; i < ar_len; i++)
			console.log(' | ', params[i])
		if (i > 3)
			process.stdout.write('\n')
	}
}
