// region Initialization
$(function () {
    /*
    for (b in RM.buttons) {
        console.debug('[buttons] attach: ', RM.buttons[b])
        RM.buttons[b] = eval.bind(RM.buttons[b])
    }
    */
    RMLanguages.initializeLanguage();
});

$(document).on(RM.Events.languageInitialized, function () {

    RM.map = L.map('mapId')
    console.debug('[map_init] created map ', RM.map)

    // TODO move to Map > events > clic > if near track/anywhere ?
    RM.map.on('click', ev => {
        console.log('[map:clic] mouse position: ', ev.latlng)
        // let textBox = RM.trackCommentBox.formatUnicorn(track).toHtmlElement();
        // const comment_marker = RM.Marker.createCommentMarker(ev.latlng)
        //comment_marker.widget.addTo(RM.map)
    });

    // add osm layers
    RM.mapLayers = [], RM.mapLayersControls = {}
    for (layer in RM.MapTypes) {
        // console.debug('...	' + layer, RM.MapTypes[layer])
        computed_layer = L.tileLayer(RM.MapTypes[layer].tiles, RM.MapTypes[layer].info)
        RM.mapLayers.push(computed_layer)
        RM.mapLayersControls[layer] = computed_layer
    }
    computed_layer.addTo(RM.map)
    console.debug('[map_init] layers : ', RM.mapLayers)

    // center default view & show visible tracks
    RM.map.setView(RM.defaultLocation, RM.defaultZoomLevel)
        .on('moveend', function () {
            RM.showMarkersWihtinXKm($('#maxDistance').val());
        });
    console.debug('[map_init] view & dynamic tracks show enabled', RM.map)

    // add layers controls
    controls = L.control.layers(RM.mapLayersControls, null, {position: 'bottomleft'})
    controls.addTo(RM.map)
    console.debug('[map_init] added layers control ', controls)


    /* create customs controls */
    for (index in RM.buttons) {
        let control_extended = L.Control.extend({
            onAdd: function(map) {
                let text = L.DomUtil.create('button')
                text.innerHTML = index
                text.classList = 'btn btn-default leaflet-control-layers leaflet-control'
                L.DomEvent.disableClickPropagation(text)
                console.debug('[buttons] onclick: ', text, RM[RM.buttons[index]])
                L.DomEvent.on(text, 'click', RM[RM.buttons[index]])
                return text
            }
        })
        control = new control_extended({position: 'topleft'})
        control.addTo(RM.map)
    }


    // load tracks
    console.info('[map_init] start loading GPX datas')
    RM.loadGpx(RM.api + '/tracks').then(
        tracklist => {
            init_tracklist(tracklist)
            console.info('[loadGpx] successfully downloaded datas ', tracklist)
        }, err => console.error('[loadGpx] error: ', err)
    )

    /*
    const riderAgreement = RM.RiderLocationAgreement();
    if (riderAgreement === 'true') {
        RM.centerMapOnRiderLocation();
    } else if (riderAgreement === null) {
        RM.askForRiderLocation();
    }
    */
});

// endregion

// region Map
RM.centerMapOnRiderLocation = function () {
    if (RM.riderLocation !== undefined && RM.riderLocation !== null) {
        RM.centerMapOn(RM.riderLocation);
    }
    else if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            RM.riderLocation = [
                position.coords.latitude,
                position.coords.longitude
            ];
            /*
            RM.displayMarker(RM.createMarker(RM.riderLocation, RM.l.yourLocation, {
                icon: L.icon(RM.mapIcons.RiderLocation)
            }));
            */
            RM.centerMapOn(RM.riderLocation);
        });
    }
};

/*
 * data is either a GPX object (from leaflet GPX plugin)
 * or a array of latlng:
 * [ [lat, lng], [lat, lng] ]
 *
 * doc: 
var corner1 = L.latLng(40.712, -74.227),
corner2 = L.latLng(40.774, -74.125),
    bounds = L.latLngBounds(corner1, corner2);
    */
RM.centerMap= function (data, screen_padding=5) {
    // TODO default latlng : current/saved/last position
    const pad = (RM.map.getSize()).x / screen_padding;
    position = typeof(data.getBounds) === 'function' ? data.getBounds() : data;
    RM.map.fitBounds(position, {paddingBottomRight: L.point([pad, 0])});
};

RM.centerMapOn = function (latlng) {
    // TODO offset
    RM.map.setView(latlng, RM.map.zoom);
};

RM.rotateMap = function(deg) {
    RM.mapRotation += deg

    let map = document.getElementById('mapId').firstElementChild
    x = map.parentElement.clientWidth / 2; y = map.parentElement.clientHeight / 2;

    console.info("[rotateMap] rotate by " + RM.mapRotation + "deg in x,y: " + x + ', ' + y)
    map.style.transformOrigin = x+'px '+y+'px';
    map.style.transform = 'rotate('+RM.mapRotation+'deg)'

    document.getElementById('mapRotation').innerHTML = RM.mapRotation
}

RM.clearMap = function() {
    if ((RM.redrawVisibleTracks = !RM.redrawVisibleTracks))
        return RM.showTracksWithinXKM();
    for (const [ignored,track] of iterate_object(RM.tracks)) {
        if (track.widget !== undefined && track.widget !== null) {
            track.widget.removeFrom(RM.map);
        }
        if (track.widget !== undefined && track.widget !== null) {
            track.widget.remove();
        }
    }
};
// endregion

// region Tracks
RM.getTrackIdFromTrackNumber = function(trackNumber) {
    return Templates.tracks.trackIdSuffix + trackNumber;
};

RM.getTrack = function(idOrNumberOrTrack) {
    /*
    let trackId = idOrNumberOrTrack;
    let track = idOrNumberOrTrack;
    if (typeof idOrNumberOrTrack === "number") {
        trackId = RM.getTrackIdFromTrackNumber(idOrNumberOrTrack);
        track = RM.tracks[trackId];
    } else if (typeof idOrNumberOrTrack === "string") {
        track = RM.tracks[trackId];
    }
    */
    return RM.tracks[idOrNumberOrTrack];
};

RM.loadGpx = function (url) {
    return new Promise((resolve, reject) => {
        const xhr = window.XMLHttpRequest ?
            new XMLHttpRequest() :
            new ActiveXObject('Microsoft.XMLHTTP');

        /*
    if (xhr.overrideMimeType) {
        xhr.overrideMimeType('text/xml');
    }
    */

        /* TODO generic upload function */
        xhr.onerror = reject
        xhr.open('GET', url, true);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
               //  resolve(xhr.response)
                resolve(JSON.parse(xhr.response))
            }
            reject(xhr)
        }
        xhr.send();
    })
};

RM.goToTrack = function (track_id, hide_menu=false) {
    const track = RM.tracks[track_id]
    console.debug('[goToTrack] track id ', track_id)

    if (!track.displayed)
        track.toggleDisplay()
    track.focus_on_map()
    if (!hide_menu)
        RM.trackActions.view(track)
};

RM.addTrackToList = function (track) {
    if (track.inList === true) {
        console.debug('[addTrackToList] tried to show a track already in list');
        return;
    }
    if (!RM.l.tracklist) {
        RM.l.tracklist = Templates.tracks.trackList.formatUnicorn().toHtmlElement()
        RM.l.tracklist_ul = RM.l.tracklist.getElementsByTagName('ul')[0]
    }
    const textBox = Templates.tracks.trackBoxListItem.formatUnicorn(track.fu).toHtmlElement()
    track.inList = true;
    RM.l.tracklist_ul.appendChild(textBox)
};

RM.removeTrackFromList = function(track_id) {
    const track = RM.tracks[track_id]
    if (!track.inList) {
        return ;
    }
    if (!RM.l.tracklist) {
        RM.l.tracklist = Templates.tracks.trackList.formatUnicorn().toHtmlElement()
        return ;
    }
    const track_box = document.getElementById('desc_track_' + track_id);
    console.debug('[removeTrackFromList] elem: ', track_box)
    if (track_box)
        track_box.parentElement.removeChild(track_box)
    track.inList = false;
}

// html_elem -> trakc/form/..?
RM.switchView = function(html_elem=null) {
    if (!RM.l.view_container) {
        RM.l.container = document.getElementById('views_container')
    }
    let child = null;
    while( (child = RM.l.container.firstElementChild) )
        RM.l.container.removeChild(child)
    if (html_elem) {
        RM.l.container.appendChild(html_elem)
    } else if (RM.l.tracklist) {
        RM.l.container.appendChild(RM.l.tracklist)
    }
}

RM.getElevations = function(gpx) {
    gain = gpx.get_elevation_gain()
    loss = gpx.get_elevation_loss()
    return [gain, loss]
}

RM.showTracksWithinXKM = function(x) {
    for (const [ignored,track] of iterate_object(RM.tracks)) {
        const distance = RM.map.distance(RM.map.getCenter(), track.start_position);
        if (x === undefined  || x === null || distance <= x*1000) {
            track.widget.addTo(RM.map);
        } else {
            track.widget.remove();
            if (track.widget !== null) {
                track.widget.remove();
            }
        }
    }
};
// endregion

// region Tracks Edition

/**
 * create a Track creation form
 * attach upload handler to form validation
 * attach track viewer on file upload
 */
RM.uploadTrack = function () {
    const inputs = [
        { type: 'text', name: 'title', id: 'new_track_title', label: RM.l.title },
        { type: 'select', name: 'type', id: 'new_track_type', label: RM.l.type, choices: Object.keys(RM.RideTypes) },
        { type: 'select', name: 'difficulty', id: 'new_track_difficulty', label: RM.l.difficulty, choices: Object.keys(RM.RideDifficulty) },
        { type: 'file', name: 'gpx', id: 'new_track_gpx', label: RM.l.gpx_file }
    ]
    console.debug('[uploadTrack] will upload ', inputs)
    const handle_form = function(e) { console.log('handler called'); RM.prepareTrack(e); RM.switchView(); }
    const [form, submit] = Templates.Forms.createForm('uploadTrackForm', {url: RM.api + '/track', func: handle_form}, inputs)
	let closeButton = Templates.closeView.formatUnicorn().toHtmlElement()
    form.appendChild(submit)
    form.appendChild(closeButton)
    console.debug('[uploadTrack] form: ', form)
    RM.switchView(form)
    document.getElementById('new_track_gpx').addEventListener('change', (ev) => {
        console.debug('[uploadTrack] gpx loaded: ', ev)
        RM.liveViewNewTrack(ev);
        // track = RM.prepareTrack(ev)
        // RM.addTrackToList(track)
        // RM.gotoTrack(track)
    })
}

/**
 * read form event and return a Track
 */
RM.prepareTrack = function (e) {
    console.info('[prepareTrack] track: ', e)
    let file = null
    /* TODO move event default handler cancelation ? */
    e.preventDefault()
    e.stopPropagation()

    let form_elements = e.target.elements
    let tmp_track = {}
    let l = form_elements.length
    let reader = new FileReader()

    /* todo model ? */
    for (i = 0; i < l; i++) {
        if (form_elements[i].localName == "input" || form_elements[i].localName == "select") {
            if (!(form_elements[i].name === "gpx"))
                tmp_track[form_elements[i].name] = form_elements[i].value
            else
                file = form_elements[i].files[0]
        }
    }

    const track = new RM.Track(
        tmp_track.title,
        null,
        tmp_track.difficulty,
        tmp_track.type
    )

    reader.onload = function(event) {
        track.string = reader.result

        uploadTrack(track, function (resp) {
            track.id = resp.track_id
            RM.tracks[track.id] = track
            console.debug('[prepareTrack] add and show track on map: ', track)
            RM.addTrackToList(track)
            RM.goToTrack(track.id)
        })
    }
    if (file) {
        reader.readAsText(file)
    } else {
        uploadTrack(track, r => {
            track.id = resp.track_id
            RM.tracks[track.id] = track
        })
    }
}

RM.liveViewNewTrack = function(ev) {
    const track_gpx = ev.target.files[0],
        reader = new FileReader()

    if (!track_gpx) {
        console.error('[liveViewNewTrack] no track in : ', ev)
        return
    }

    reader.onload = function(event) {
        // const xml = (new DOMParser).parseFromString(reader.result, 'text/xml')
        const track = new RM.Track('tmp', reader.result, 'easy', 'other')
        console.log(track)
        track.focus_on_map();
    }
    reader.readAsText(track_gpx)
}


// endregion


// region Markers
RM.showMarkersWihtinXKm = function (x) {
    if (!RM.redrawVisibleTracks)
        return;
    for (const [ignored,track] of iterate_object(RM.tracks)) {
        const distance = RM.map.distance(RM.map.getCenter(), track.start_position);
        if (x === undefined  || x === null || distance <= x*1000) {
            track.widget.addTo(RM.map);
        } else {
            track.widget.remove();
            if (track.widget !== null) {
                track.widget.remove();
            }
        }
    }
};
// endregion

// region Rider's location
RM.askForRiderLocation = function() {
    if (RM.RiderLocationAgreement() === true) {
        RM.centerMapOnRiderLocation();
        return;
    }
    const actions = [
        [RM.l.accept, RM.acceptRiderLocation],
        [RM.l.decline, RM.declineRiderLocation]
    ];
    /* ? */
    Templates.Modals.displayModal(Templates.Modals.createModal(RM.l.uploadTrack, RM.l.authorizeRiderLocation, 'locationModal', actions));
};

RM.acceptRiderLocation = function() {
    RM.RiderLocationAgreement(true);
    RM.centerMapOnRiderLocation();
};

RM.declineRiderLocation = function() {
    RM.RiderLocationAgreement(false);
};
// endregion

// region initial data loading

function init_tracklist(tracklist) {
    let tmp_track, track;
    // const tracklist = msgpack.decode(new Uint8Array(rtracklist));
    console.debug('[init_tracklist] loading: ', tracklist)
    for(i in tracklist) {
        tmp_track = tracklist[i];
        track = new RM.Track(
            tmp_track.title,
            tmp_track.xml,
            tmp_track.difficulty,
            tmp_track.type,
            tmp_track.id
        );
        RM.addTrackToList(track)
        if (track.id)
            RM.tracks[track.id] = track;
    }
    RM.switchView()
}

// endregion

/*
 * markers actions
 * to move ?
 */

RM.trackActions = {
    view: function(track) {
        RM.switchView(Templates.tracks.trackFullView.formatUnicorn(track.fu).toHtmlElement())
    }
}

RM.filterTracklist = function(search) {
    console.debug('[search] will search: ', search)
    let track_text = null, i = null
    for (t_index in RM.tracks) {
        t = RM.tracks[t_index]
        i = t.informations
        track_text = [t.name, t.difficulty, t.type, t.title].join().toLowerCase()
        if (track_text.indexOf(search) === -1) {
            console.debug('[search] no    ', track_text)
            RM.removeTrackFromList(t.id)
        } else {
            console.debug('[search] match ', track_text)
            RM.addTrackToList(t)
        }
    }
}

RM.delayFilterTrack = function(ev) {
    if (!this.timeOut) {
        this.timeOut = setTimeout(RM.filterTracklist.bind(null, ev.value), 200)
    } else {
        clearTimeout(this.timeOut)
        this.timeOut = setTimeout(RM.filterTracklist.bind(null, ev.value), 200)
    }
}

/*
RM.buttons.dispatchEvents = function(ev) {
    console.log('OK EVENT dISPATCH', ev)
}
*/

RM.showGPX = function(gpx) {
    let elem = new L.GPX(gpx, {
        marker_options: {
            startIconUrl: true,
            endIconUrl: true
        }
    })
    elem.addTo(RM.map)
}
