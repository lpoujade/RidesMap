/*
L.Control.Watermark = L.Control.extend({
    onAdd: function(map) {
        var img = L.DomUtil.create('img');

        img.src = '../../docs/images/logo.png';
        img.style.width = '200px';

        return img;
    },

    onRemove: function(map) {
        // Nothing to do here
    }
});

L.control.watermark = function(opts) {
    return new L.Control.Watermark(opts);
}

L.control.watermark({ position: 'bottomleft' }).addTo(map);
 */

RM.Marker = class {
    constructor(latlng, opts=null) {
        console.debug('[RM.Marker] new marker at ', latlng)
        console.debug('[RM.Marker] ... with opts', opts)
        this.latlng = latlng;
        this.widget   = L.marker(latlng, (opts && opts.properties) ? opts.properties : null)

        if (opts && opts.action) {
            console.debug('[RM.Marker] add opts.action: ', opts.action)
            this.widget.on(opts.action.ev, opts.action.func)
        }

        /*
        if (opts && opts.html) {
            const textBox = RM.startMarkerBox.formatUnicorn(trackInformations).toHtmlElement()
            console.info('TODO attach textBox')
        }
        */
    }

    // region Public members
    isDisplayed() {
        return this.widget._map !== null;
    }
    // endregion

    // region Static members
    static createStartMarker(latlng, track_type, clic_callback) {
        console.debug('[createMarker] for ', latlng, track_type)
        if (!latlng || !track_type) {
            console.error('[Marker createStartMarker] missing one of start_position, or type: ', latlng, track_type)
            return null;
        }
        return new RM.Marker(latlng, {
                properties : {icon: L.icon(RM.mapIcons[track_type])},
                action: {ev: 'click', func: clic_callback}
            });
    }

    static createCommentMarker  (latlng) {
        const log = function(ev) {
            console.info('TODO show in comment box')
            console.info('TODO stick marker on GPX, not on mouse/ev coordinates')
        }
        return new RM.Marker(latlng, {
            properties: {icon: L.icon(RM.mapIcons.comment)},
            action: {ev: 'click', func: log}
        })
    }
    // endregion
};
