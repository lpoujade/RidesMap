RM.DisplayedTrack = class {
    constructor(track) {
        this.track       = track                    ;
        this.trackWidget = undefined                ;
        this.startWidget = this.computeStartWidget();
    }

    // region Public members
    addTo(map) {
        if (this.trackWidget === undefined || this.trackWidget === null) {
            this.computeTrackWidget();
        }
        this.trackWidget.addTo(map);
    }

    remove() {
        if (this.trackWidget !== undefined && this.trackWidget !== null) {
            this.trackWidget.remove();
        }
    }

    isDiplayed() {
        return this.trackWidget      !== undefined
            && this.trackWidget      !== null
            && this.trackWidget._map !== null     ;
    }
    // endregion

    // region Private members
    computeTrackWidget() {
        this.trackWidget = new L.GPX(this.track.string,
            {
                async: false,
                polyline_options: this.track.type.style,
                marker_options: {
                    startIconUrl: false,
                    endIconUrl: false
                }
            }
        );
    }

    computeStartWidget() {
        this.startWidget = RM.Marker.createStartMarker(this.trackWidget.trackInformations);
    }
    // endregion
};