/*
Define main scope and asynchronously load js files when page is loaded.
 */


/* TODO put this in env/whatever */
const RM = {};
RM.appName = 'RidesMap';
//RM.api = 'https://api.ridesmap.lpo.host'
//RM.api = 'http://10.42.0.1:5000'
RM.api = 'http://localhost:5000'

RM.filesToLoad = [
    "js/Models/trackStyle.js"       ,
    "js/Models/trackType.js"        ,
    "js/Models/trackInformations.js",
    "js/Models/track.js"            ,
    "js/Views/marker.js"            ,
    "js/Views/displayedTrack.js"    ,
    "js/Templates/templates.js"     ,
    "js/Templates/modals.js"        ,
    "js/Templates/forms.js"         ,
    "js/Templates/tracks.js"        ,
    "js/languages.js"               ,
    "js/upload.js"                  ,
    "js/tools.js"                   ,
    "js/values.js"                  ,
    "js/core.js"
];

RM.loadFiles = function(head, index) {
    let script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = RM.filesToLoad[index];

    script.onload = function() {
        if (!RM.filesToLoad[index+1]) return;
        RM.loadFiles(head, index+1);
    };

    head.appendChild(script);
};

$(function() {
    RM.loadFiles($('head')[0], 0);
});
