function uploadTrack(track, onSuccessCallback) {
	let xhr = window.XMLHttpRequest ?
		new XMLHttpRequest() :
		new ActiveXObject('Microsoft.XMLHTTP');

	xhr.onerror = function (e) { console.error('[upload] ---- XHR error', e) }
	console.info('[upload] request to RM api: ', RM.api + 'track') 
	xhr.open('POST', RM.api + '/track', true);
	xhr.onload = function () {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				resp = JSON.parse(xhr.responseText)
				onSuccessCallback(resp);
			} else
				console.error('[upload] -- HTTP error ', '', ' ---- ', xhr.statusText);
		} else
			console.error('[upload] -- XHR error', '', xhr);
	};
	xhr.send(track.toJson)
}
