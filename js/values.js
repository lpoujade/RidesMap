// TODO move ?
RM.mapRotation = 0;
RM.redrawVisibleTracks = true;
/* https://leaflet-extras.github.io/leaflet-providers/preview/ */
// region Enums
RM.MapTypes = {
    France: {
        tiles: 'https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png',
        info: {
            maxZoom: 20,
            id: 'france',
            attribution: '&copy; Openstreetmap France | &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }
    },
    TopoMap: {
        tiles: 'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
        info: {
            maxZoom: 17,
            id: 'topomap',
            attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
        }
    },
    OpenCycleMap: { tiles: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png', infos: {}},
    HOTOSM: { tiles: 'https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png' },
    HikeBike: { tiles: 'https://toolserver.org/tiles/hikebike/{z}/{x}/{y}.png', infos: {}},
    Watercolor: { tiles: 'http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.jpg', infos: {}},
    dark: { tiles: 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png', infos: {}},
    worldimagery: {
        tiles: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
        infos: {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
        }
    }
};
RM.RideTypes = {
    road: {
        style: {
            color: 'blue',
            opacity: 0.75,
            weight: 3,
            lineCap: 'round'
        }
    },
    cross: {
        style: {
            color: 'orangered',
            opacity: 0.75,
            weight: 3,
            lineCap: 'round'
        }
    },
    muni: {
        style: {
            color: 'green',
            opacity: 0.75,
            weight: 3,
            lineCap: 'round'
        }
    },
    other: {
        style: {
            color: 'white',
            opacity: 0.75,
            weight: 3,
            lineCap: 'round'
        }
    }
};

RM.RideDifficulty = {
    easy: {},
    medium: {},
    hard: {}
};

RM.Events = {
    languageInitialized: 'languageInitialized'
};
// endregion

// region Default values
RM.defaultLocation = [47.070122, 2.636719];
RM.defaultZoomLevel = 7;
// endregion

// region Constants

RM.mapIcons = {
    comment: {
        iconUrl: '/img/marker-comment.png',
        iconAnchor: [10, 25],
        iconSize: [20, 20]
    },
    riderLocation: {iconUrl: 'img/black-marker-icon.png'},
    muni: {iconUrl: 'img/black-marker-icon.png'},
    cross: {iconUrl: 'img/black-marker-icon.png'},
    road: {iconUrl: 'img/black-marker-icon.png'}
}

// view_visibles_tracks: 'showMarkersWihtinXKm',
// rotate_map: 'rotateMap',
RM.buttons = {
    center_map: 'askForRiderLocation',
    'hide/show tracks': 'clearMap',
    add_track: 'uploadTrack',
}

// region Global variables
RM.map = null;
RM.riderLocation = null;
RM.tracks = {};
RM.l = RMLanguages.defaultLanguage;
// endregion

// region Local storage access

// Later functions are double accessors:
// no-arg-call is a getter where arg-call is a setter.
RM.RiderLocationAgreement = function(agreement) {
    const key = 'RM.RiderLocationAgreement';
    return RM.accessStorage(key, agreement, {getterMutator: agreement => agreement || agreement === "true"})
};

RM.Language = function(language) {
    const key = 'RM.Language.name';
    return RM.accessStorage(key, language,
        {
            setterMutator: language => language.englishLanguageName,
            getterMutator: language => RMLanguages.Dictionnaries[language]
        });
};

RM.accessStorage = function(key, value, {setterMutator, getterMutator}) {
    setterMutator = setterMutator || (valueToStore => valueToStore);
    getterMutator = getterMutator || (storedValue => storedValue);
    if (value === undefined) return getterMutator(localStorage.getItem(key));
    else localStorage.setItem(key, setterMutator(value));
};
// endregion

