RMLanguages = {};

// region Dictionnaries
RMLanguages.Dictionnaries = {};
RMLanguages.Dictionnaries.English = {
    englishLanguageName: 'English',
    localLanguageName: 'English',
    accept: 'Accept',
    decline: 'Decline',
    location: 'Location',
    yourLocation: 'Your location',
    authorizeRiderLocation: `Would you like to authorize ${RM.appName} to access to your location?`,
    centerMyLocation: 'Center on my location',
    clearMap: 'Clear map',
    showTracks: 'Show tracks',
    welcome: 'Welcome',
    chooseLanguage: 'Please choose a language',
    uploadTrack: 'Upload a track',
	title: 'Title', difficulty: 'Difficulty', gpx_file: 'GPX file', send: 'Send',
	type: 'Type'
};

RMLanguages.Dictionnaries.French = {
    englishLanguageName: 'French',
    localLanguageName:'Français',
    accept: 'Accepter',
    decline: 'Refuser',
    location: 'Position',
    yourLocation: 'Votre position',
    authorizeRiderLocation: `Autorisez-vous ${RM.appName} à accéder à votre position ?`,
    centerMyLocation: 'Centrer la carte sur ma position',
    clearMap: 'Tout effacer',
    showTracks: 'Montrer les chemins',
    welcome: 'Bienvenue',
    chooseLanguage: 'Veuillez choisir un langage',
    uploadTrack: 'Ajouter un circuit',
	title: 'Titre', difficulty: 'Difficulté', gpx_file: 'Fichier GPX', send: 'Envoyer',
	type: 'Type'
};
// endregion

RM.defaultLanguage = RMLanguages.Dictionnaries.English;

RMLanguages.firstVisit = function() {
    const l = RM.defaultLanguage;

    RM.Language(l);
    RMLanguages.initializeLanguage();

    return ;
    let action = [l.accept, function(value) {
        RM.Language(RMLanguages.getLanguageByLocalName(value));
        RMLanguages.initializeLanguage();
    }];
    Templates.Modals.displayModal(Templates.Modals.createModalWithSelectList(l.welcome, l.chooseLanguage, 'chooseLanguageModal', RMLanguages.listAvailableLanguages(), [action]))
};

RMLanguages.initializeLanguage = function () {
    RM.l = RM.Language();
    if (RM.l === undefined || RM.l === null) {
        RMLanguages.firstVisit();
        return;
    }

    const elementsToTranslate = $('[data-language-key]');
    elementsToTranslate.each(function(_, element) {
        const jElement = $(element);
        jElement.empty();
        jElement.append(RM.l[jElement.data('language-key')]);
    });

    $(document).trigger(RM.Events.languageInitialized);
};

RMLanguages.listAvailableLanguages = function() {
    let languages = [];
    for(const [ignored,language] of iterate_object(RMLanguages.Dictionnaries)) {
        languages.push(language.localLanguageName);
    }
    return languages;
};

RMLanguages.getLanguageByLocalName = function(localName) {
    for (const [ignored, language] of iterate_object(RMLanguages.Dictionnaries)) {
        if (localName === language.localLanguageName) {
            return language;
        }
    }
    return null;
};
